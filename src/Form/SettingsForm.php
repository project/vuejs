<?php

namespace Drupal\vuejs\Form;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Vue.js settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * Constructs a form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config.factory service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   The library.discovery service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module_handler service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    protected LibraryDiscoveryInterface $libraryDiscovery,
    protected ModuleHandlerInterface $moduleHandler,
  ) {
    parent::__construct($configFactory, $typedConfigManager);
    $this->libraryDiscovery = $libraryDiscovery;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('library.discovery'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'vuejs_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['vuejs.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $vueSettings = $this->config('vuejs.settings')->get('vue');

    // Change the text based on whether the Help module is available.
    $pathDescription = $this->t('The path to the library file. Leave empty to disable the library');
    if ($this->moduleHandler->moduleExists('help')) {
      $pathDescription = $this->t('The path to the library file. See <a href=:link>here</a> for more infos on how to setup the vue library locally. Leave empty to disable the library.', [':link' => Url::fromRoute('help.page', ['name' => 'vuejs'])->toString()]);
    }

    $form['vue_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Vue.js library'),
      '#default_value' => $vueSettings['enabled'],
      '#description' => $this->t('This will enabled the Vue.js library.'),
    ];
    $form['vue'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('VueJS Runtime'),
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="vue_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['vue']['installation'] = [
      '#type' => 'select',
      '#title' => $this->t('Installation Type'),
      '#options' => [
        'local' => $this->t('Local library'),
        'cdn' => $this->t('Use an external CDN'),
      ],
      '#default_value' => $vueSettings['installation'],
      '#required' => TRUE,
    ];
    $form['vue']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Library path'),
      '#default_value' => $vueSettings['path'],
      '#attributes' => [
        'placeholder' => '/libraries/vue/package/dist/vue.global.prod.js',
      ],
      '#description' => $pathDescription,
      '#states' => [
        'visible' => [
          'select[name="vue[installation]"]' => ['value' => 'local'],
        ],
        'required' => [
          'select[name="vue[installation]"]' => ['value' => 'local'],
        ],
      ],
    ];
    $form['vue']['cdn_provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Select a CDN provider'),
      '#options' => [
        'unpkg' => $this->t('UNPKG'),
        'cdnjs' => $this->t('cdnjs'),
        'jsdelivr' => $this->t('jsDelivr'),
      ],
      '#default_value' => $vueSettings['cdn_provider'],
      '#states' => [
        'visible' => [
          'select[name="vue[installation]"]' => ['value' => 'cdn'],
        ],
      ],
    ];
    $form['vue']['cdn_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Version'),
      '#size' => 9,
      '#default_value' => $vueSettings['cdn_version'],
      '#states' => [
        'visible' => [
          'select[name="vue[installation]"]' => ['value' => 'cdn'],
        ],
      ],
    ];

    $petiteVueSettings = $this->config('vuejs.settings')->get('petitevue');

    $form['petitevue_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Petite vue library'),
      '#default_value' => $petiteVueSettings['enabled'],
      '#description' => $this->t('This will enabled the Petite vue library.'),
    ];
    $form['petitevue'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Petite Vue Runtime'),
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="petitevue_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['petitevue']['installation'] = [
      '#type' => 'select',
      '#title' => $this->t('Installation Type'),
      '#options' => [
        'local' => $this->t('Local library'),
        'cdn' => $this->t('Use an external CDN'),
      ],
      '#default_value' => $petiteVueSettings['installation'],
      '#required' => TRUE,
    ];
    $form['petitevue']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Library path'),
      '#attributes' => [
        'placeholder' => '/libraries/petitevue/package/dist/petite-vue.iife.js',
      ],
      '#default_value' => $petiteVueSettings['path'],
      '#description' => $pathDescription,
      '#states' => [
        'visible' => [
          'select[name="petitevue[installation]"]' => ['value' => 'local'],
        ],
        'required' => [
          'select[name="vue[installation]"]' => ['value' => 'local'],
        ],
      ],
    ];
    $form['petitevue']['cdn_provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Select a CDN provider'),
      '#options' => [
        'unpkg' => $this->t('UNPKG'),
        'cdnjs' => $this->t('cdnjs'),
        'jsdelivr' => $this->t('jsDelivr'),
      ],
      '#default_value' => $petiteVueSettings['cdn_provider'],
      '#states' => [
        'visible' => [
          'select[name="petitevue[installation]"]' => ['value' => 'cdn'],
        ],
      ],
    ];

    $form['petitevue']['cdn_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Version'),
      '#size' => 9,
      '#default_value' => $petiteVueSettings['cdn_version'],
      '#states' => [
        'visible' => [
          'select[name="petitevue[installation]"]' => ['value' => 'cdn'],
        ],
      ],
    ];
    $form['petitevue']['defer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Defer loading'),
      '#default_value' => $petiteVueSettings['defer'],
      '#desctiption' => $this->t('This will defer the loading of the library until the page has finished loading.'),
    ];
    $form['petitevue']['init'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Initialize'),
      '#default_value' => $petiteVueSettings['init'],
      '#desctiption' => $this->t('This will initialize PetiteVue instances on the page automatically. See <a href=:link>here</a> for more infos.', [':link' => Url::fromUri('https://github.com/vuejs/petite-vue#manual-init')]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // We are using these funky keys, because "setErrorByName" only accepts
    // strings unfortunately. So we can not use the form values directly:
    $versionArray = [
      'vue][cdn_version' => $form_state->getValue(['vue', 'cdn_version']),
      'petitevue][cdn_version' => $form_state->getValue(['petitevue', 'cdn_version']),
    ];
    // Validate if the versions are in the correct format:
    foreach ($versionArray as $formName => $formValue) {
      if (!preg_match('/^(?<prefix>v)?(?<version>\d+\.\d+\.\d+(?:-(?:alpha|beta|rc)\.\d)?)$/', $formValue)) {
        $form_state->setErrorByName($formName, $this->t('Version format is incorrect.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the vue form values:
    $vueFormValues = $form_state->getValue('vue');
    $enableVueLibrary = $form_state->getValue('vue_enabled');

    // Set the path for the given CDN.
    if ($vueFormValues['installation'] === 'cdn' && (bool) $enableVueLibrary) {
      // The file name differs from Vue 2 to Vue 3, so we need to do a version
      // compare here:
      $fileName = version_compare($vueFormValues['cdn_version'], '3.0.0', '>=') ? 'vue.global.prod.js' : 'vue.min.js';
      switch ($vueFormValues['cdn_provider']) {
        case 'jsdelivr':
          $vueFormValues['path'] = '//cdn.jsdelivr.net/npm/vue@' . $vueFormValues['cdn_version'] . '/dist/' . $fileName;
          break;

        case 'cdnjs':
          $vueFormValues['path'] = '//cdnjs.cloudflare.com/ajax/libs/vue/' . $vueFormValues['cdn_version'] . '/' . $fileName;
          break;

        case 'unpkg':
        default:
          $vueFormValues['path'] = '//unpkg.com/vue@' . $vueFormValues['cdn_version'] . '/dist/' . $fileName;
      }
    }

    // Check the set settings and update config as necessary.
    $petiteVueFormValues = $form_state->getValue('petitevue');
    $enablePetiteVueLibrary = $form_state->getValue('petitevue_enabled');

    // Set the path for the given CDN.
    if ($petiteVueFormValues['installation'] == 'cdn' && (bool) $enablePetiteVueLibrary) {
      switch ($petiteVueFormValues['cdn_provider']) {
        case 'jsdelivr':
          $petiteVueFormValues['path'] = '//cdn.jsdelivr.net/npm/petite-vue@' . $petiteVueFormValues['cdn_version'] . '/dist/petite-vue.iife.js';
          break;

        case 'cdnjs':
          $petiteVueFormValues['path'] = '//cdnjs.cloudflare.com/ajax/libs/petite-vue/' . $petiteVueFormValues['cdn_version'] . '/petite-vue.iife.js';
          break;

        case 'unpkg':
        default:
          $petiteVueFormValues['path'] = '//unpkg.com/petite-vue@' . $petiteVueFormValues['cdn_version'] . '/dist/petite-vue.iife.js';
      }
    }
    // Add the enabled values to the form values before saving:
    $vueFormValues['enabled'] = $enableVueLibrary;
    $petiteVueFormValues['enabled'] = $enablePetiteVueLibrary;

    $this->config('vuejs.settings')
      ->set('vue', $vueFormValues)
      ->set('petitevue', $petiteVueFormValues)
      ->save();
    $this->libraryDiscovery->clearCachedDefinitions();
    parent::submitForm($form, $form_state);
  }

}
