<?php

/**
 * @file
 * Install, update and uninstall functions for the vuejs module.
 *
 * @ingroup vuejs
 */

use Drupal\Core\Url;

/**
 * Implements hook_requirements().
 */
function vuejs_requirements($phase) {
  $requirements = [];
  // If we are not in runtime phase, there is nothing to do. So bail out early.
  if ($phase !== 'runtime') {
    return [];
  }

  /** @var \Drupal\Core\Asset\LibrariesDirectoryFileFinder $library_file_finder */
  $library_file_finder = \Drupal::service('library.libraries_directory_file_finder');
  $config = \Drupal::config('vuejs.settings');
  $vueSettings = $config->get('vue');
  $petiteVueSettings = $config->get('petitevue');

  // Only show a warning, if the path is set and the library enabled:
  if (!empty($vueSettings['path']) && (bool) $vueSettings['enabled'] && $vueSettings['installation'] === 'local') {
    // Library file finder won't find the file, if it is prefixed with
    // "/libraries/", so we need to remove it:
    if (!$library_file_finder->find(str_replace('/libraries', '', $vueSettings['path']))) {
      $requirements['vuejs_vue'] = [
        'title' => t('Vue'),
        'severity' => REQUIREMENT_WARNING,
        'value' => t('Missing vue.js library'),
        'description' => t('The vue.js library is enabled, but the given path
        "@path" could not get resolved to an existing vue.js library file.', [
          '@path' => $vueSettings['path'],
        ]),
      ];
    }
  }

  if (!empty($petiteVueSettings['path']) && (bool) $petiteVueSettings['enabled'] && $petiteVueSettings['installation'] === 'local') {
    // Library file finder won't find the file, if it is prefixed with
    // "/libraries/", so we need to remove it:
    if (!$library_file_finder->find(str_replace('/libraries', '', $petiteVueSettings['path']))) {
      $requirements['vuejs_petitevue'] = [
        'title' => t('Petite Vue'),
        'severity' => REQUIREMENT_WARNING,
        'value' => t('Missing petite vue library'),
        'description' => t('The petite vue library is enabled, but the given
        path "@path" could not get resolved to an existing petite vue library
        file.', ['@path' => $petiteVueSettings['path']]),
      ];
    }
  }
  return $requirements;
}

/**
 * IMPORTANT!
 *
 * Upgrading vuejs to 3.x from 8.x-1.x resets the module settings to 3.x
 * defaults. Before running the upgrade, please take a backup and check your
 * vuejs 8.x-1.x settings. See
 * https://www.drupal.org/project/vuejs/issues/3404816 for details.
 * (This upgrade will NOT touch anything for EXISTING  vuejs 3.x installations)
 */
function vuejs_update_93001(&$sandbox) {
  $config = \Drupal::config('vuejs.settings');
  $hasVueRouter = $config->get('libraries.vue_router') !== NULL;
  $hasVueResource = $config->get('libraries.vue_resource') !== NULL;
  if ($hasVueRouter || $hasVueResource) {
    // Only reset the config, if we detect old configuration, where these keys
    // were present!
    \Drupal::service('config.installer')->installDefaultConfig('module', 'vuejs');
    return t('Reset VueJS settings to installation defaults, as the 3.x configuration is incompatible with previous versions. Check the VueJS settings page and the README!');
  }
  else {
    return t('VueJS settings configuration has already been 3.x compatible. Nothing changed.');
  }
}

/**
 * Set the new defer and init values to TRUE by default.
 */
function vuejs_update_93002(&$sandbox) {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('vuejs.settings');
  $config->set('libraries.petitevue.defer', TRUE);
  $config->set('libraries.petitevue.init', TRUE);
  $config->save(TRUE);
}

/**
 * Introduces new settings and brings old settings in the new structure.
 */
function vuejs_update_93003(&$sandbox) {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('vuejs.settings');

  // Set the vue config:
  $vueConfig = $config->get('libraries.vue');
  // Enable the vue libary for existing installations:
  $vueConfig['enabled'] = TRUE;
  $vueConfig['installation'] = $vueConfig['installation'] ?? 'cdn';
  $vueConfig['path'] = $vueConfig['path'] ?? '//unpkg.com/vue@3.5.13/dist/vue.runtime.global.prod.js';
  $vueConfig['cdn_provider'] = $vueConfig['cdn'] ?? 'unpkg';
  $vueConfig['cdn_version'] = $vueConfig['version'] ?? '3.5.13';
  // Unset old configs:
  unset($vueConfig['cdn']);
  unset($vueConfig['version']);
  unset($vueConfig['development']);
  $config->set('vue', $vueConfig);

  // Set the petite vue config:
  $petiteVueConfig = $config->get('libraries.petitevue');
  // Enable the petite libary for existing installations, they can be manually
  // disabled afterwards, we don't have any negative side effect from this:
  $petiteVueConfig['enabled'] = TRUE;
  $petiteVueConfig['installation'] = $petiteVueConfig['installation'] ?? 'cdn';
  $petiteVueConfig['path'] = $petiteVueConfig['path'] ?? '//unpkg.com/petite-vue@0.4.1/dist/petite-vue.iife.js';
  $petiteVueConfig['cdn_provider'] = $petiteVueConfig['cdn'] ?? 'unpkg';
  $petiteVueConfig['cdn_version'] = $petiteVueConfig['version'] ?? '0.4.1';
  $petiteVueConfig['defer'] = $petiteVueConfig['defer'] ?? FALSE;
  $petiteVueConfig['init'] = $petiteVueConfig['init'] ?? FALSE;
  // Unset old configs:
  unset($petiteVueConfig['cdn']);
  unset($petiteVueConfig['version']);
  unset($petiteVueConfig['development']);
  $config->set('petitevue', $petiteVueConfig);

  // Completely clear old libraries config:
  $config->clear('libraries');
  $config->save(TRUE);

  // Clear library discovery cache, so the new settings are picked up:
  \Drupal::service('library.discovery')->clearCachedDefinitions();
}

/**
 * Fixes regression.
 *
 * Introduced in https://www.drupal.org/project/vuejs/issues/3488596.
 */
function vuejs_update_93004(&$sandbox) {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('vuejs.settings');

  // Set the vue config:
  $vueConfig = $config->get('vue');
  $oldPath = $vueConfig['path'];
  $newPath = NULL;
  $changed = FALSE;

  if (str_contains($oldPath, 'vue.runtime.global.prod.js')) {
    $newPath = str_replace('vue.runtime.global.prod.js', 'vue.global.prod.js', $oldPath);
    $changed = TRUE;
  }
  elseif (str_contains($oldPath, 'vue.global.prod.js') && version_compare($vueConfig['cdn_version'], '3.0.0', '<')) {
    $newPath = str_replace('vue.global.prod.js', 'vue.min.js', $oldPath);
    $changed = TRUE;
  }
  if ($changed) {
    $config->set('vue.path', $newPath);
    $config->save(TRUE);

    // Clear library discovery cache, so the new settings are picked up:
    \Drupal::service('library.discovery')->clearCachedDefinitions();
    \Drupal::logger('vuejs')->info(t("Fixed vue.js path in the settings from %oldPath to %newPath, because the old file isn't supported. See <a href=':route'>here</a> to modify the path if the new path is undesired", [
      '%oldPath' => $oldPath,
      '%newPath' => $newPath,
      ':route' => Url::fromRoute('vuejs.settings')->toString(),
    ]));
  }

}
