# VueJs

## Introduction

The Vue.js Drupal module provides a bridge between Drupal and the Vue.js framework. This module is compatible with both Vue 2 and 3 and comes with additional support for petite vue.

## Vue

### Library installation via [Composer Merge Plugin](https://github.com/wikimedia/composer-merge-plugin) (Vue 3 only)

1. Install the composer merge-plugin via `composer require wikimedia/composer-merge-plugin`
2. Add the following entry in your composer.json:
```
"extra": {
        "merge-plugin": {
            "include": [
                "web/modules/contrib/vuejs/composer.libraries.json"
            ]
        },
      },
```
3. Update the vuejs module via composer: `composer update 'drupal/vuejs'`
4. Navigate to the `admin/config/development/vue` page.
5. Set the installation type to "Local library".
6. Set the library path to `/package/dist/vue.global.prod.js` if not already defined so.

### Library installation via manual download

#### Vue 3
1. Download `https://registry.npmjs.org/vue/-/vue-3.5.13.tgz`
2. Extract the tar inside the drupal `libraries/vue` folder.
3. Navigate to the `admin/config/development/vuejs` page.
4. Set the installation type to "Local library".
5. Set the library path to `/libraries/vue/package/dist/vue.global.prod.js` if not already defined so.

#### Vue 2
1. Download `https://registry.npmjs.org/vue/-/vue-2.7.16.tgz`
2. Extract the tar inside the drupal `libraries/vue` folder.
3. Navigate to the `admin/config/development/vuejs` page.
4. Set the installation type to "Local library".
5. Set the library path to `/libraries/vue/package/dist/vue.min.js` if not already defined so.

### Library Installation via CDN

1. Navigate to the `admin/config/development/vuejs` page.
2. Set the installation type to "Use an external CDN".
3. Select your prefered provider and Version.

### USAGE

1. You can use it inside Twig templates as usual, for example:
```twig
{{ attach_library('vuejs/vue') }}
```
2. You can attach it programmatically:
```php
function MYMODULE_page_attachments(array &$attachments) {
  $attachments['#attached']['library'][] = 'vuejs/vue';
}
```
3. You can add it as a dependency inside your `*.libraries.yml`:
```yaml
dependencies:
  - vuejs/vue
```

## Petite vue

### Library installation via manual download

1. Download `https://registry.npmjs.org/petite-vue/-/petite-vue-0.4.1.tgz`
2. Extract the tar inside the drupal `libraries/petitevue` folder.
3. Navigate to the `admin/config/development/vuejs` page.
4. Set the installation type to "Local library".
5. Set the library path to `/libraries/petitevue/package/dist/petite-vue.iife.js` if not already defined so.

### Library Installation via CDN

1. Navigate to the `admin/config/development/vuejs` page.
2. Set the installation type to "Use an external CDN".
3. Select your prefered provider and Version.

### USAGE

1. You can use it inside Twig templates as usual, for example:
```twig
{{ attach_library('vuejs/petitevue') }}
```
2. You can attach it programmatically:
```php
function MYMODULE_page_attachments(array &$attachments) {
  $attachments['#attached']['library'][] = 'vuejs/petitevue';
}
```
3. You can add it as a dependency inside your `*.libraries.yml`:
```yaml
dependencies:
  - vuejs/petitevue
```

## PROJECT PAGE

For more information, visit the project page at https://www.drupal.org/project/vuejs
